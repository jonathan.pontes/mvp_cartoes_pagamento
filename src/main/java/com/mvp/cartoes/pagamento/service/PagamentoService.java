package com.mvp.cartoes.pagamento.service;

import com.mvp.cartoes.pagamento.cartao.CartaoDto;
import com.mvp.cartoes.pagamento.cartao.CartaoServiceClient;
import com.mvp.cartoes.pagamento.models.Pagamento;
import com.mvp.cartoes.pagamento.repository.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoServiceClient cartaoService;

    public Pagamento create(Pagamento pagamento) {
        CartaoDto cartaoDto = cartaoService.getById(pagamento.getCartaoId());
        pagamento.setCartaoId(cartaoDto.getId());

        return pagamentoRepository.save(pagamento);
    }

    public List<Pagamento> listByCartao(String cartaoNumero) {
        CartaoDto cartaoDto = cartaoService.getByNumero(cartaoNumero);

        return pagamentoRepository.findAllByCartaoId(cartaoDto.getId());
    }

}
