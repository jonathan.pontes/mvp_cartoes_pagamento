package com.mvp.cartoes.pagamento.controller;

import com.mvp.cartoes.pagamento.models.Pagamento;
import com.mvp.cartoes.pagamento.models.dto.CreatePagamentoRequest;
import com.mvp.cartoes.pagamento.models.dto.PagamentoResponse;
import com.mvp.cartoes.pagamento.models.mapper.PagamentoMapper;
import com.mvp.cartoes.pagamento.service.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private PagamentoMapper pagamentoMapper;

    @PostMapping("/pagamento")
    public PagamentoResponse create(@RequestBody CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = pagamentoMapper.toPagamento(createPagamentoRequest);
        pagamento = pagamentoService.create(pagamento);

        return pagamentoMapper.toPagamentoResponse(pagamento);
    }

    @GetMapping("/pagamentos/{cartaoNumero}")
    public List<PagamentoResponse> listByCartao(@PathVariable String cartaoNumero) {
        List<Pagamento> pagamentos = pagamentoService.listByCartao(cartaoNumero);
        return pagamentoMapper.toPagamentoResponse(pagamentos);
    }

}
