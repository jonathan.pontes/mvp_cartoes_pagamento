package com.mvp.cartoes.pagamento.repository;


import com.mvp.cartoes.pagamento.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PagamentoRepository extends CrudRepository<Pagamento, Long> {

    List<Pagamento> findAllByCartaoId(Long cartaoId);

}
