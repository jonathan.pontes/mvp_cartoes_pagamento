package com.mvp.cartoes.pagamento.models.mapper;


import com.mvp.cartoes.pagamento.cartao.CartaoDto;
import com.mvp.cartoes.pagamento.cartao.CartaoServiceClient;
import com.mvp.cartoes.pagamento.models.Pagamento;
import com.mvp.cartoes.pagamento.models.dto.CreatePagamentoRequest;
import com.mvp.cartoes.pagamento.models.dto.PagamentoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {

    @Autowired
    CartaoServiceClient cartaoService;

    public PagamentoResponse toPagamentoResponse(Pagamento pagamento) {
        CartaoDto cartaoDto = cartaoService.getById(pagamento.getCartaoId());

        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartaoId(cartaoDto.getId());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());

        return pagamentoResponse;
    }

    public List<PagamentoResponse> toPagamentoResponse(List<Pagamento> pagamentos) {
        List<PagamentoResponse> pagamentoResponses = new ArrayList<>();

        for (Pagamento pagamento : pagamentos) {
            pagamentoResponses.add(toPagamentoResponse(pagamento));
        }

        return pagamentoResponses;
    }

    public static Pagamento toPagamento(CreatePagamentoRequest createPagamentoRequest) {
        Pagamento pagamento = new Pagamento();

        pagamento.setDescricao(createPagamentoRequest.getDescricao());
        pagamento.setValor(createPagamentoRequest.getValor());
        pagamento.setCartaoId(createPagamentoRequest.getCartaoId());

        return pagamento;
    }
}
