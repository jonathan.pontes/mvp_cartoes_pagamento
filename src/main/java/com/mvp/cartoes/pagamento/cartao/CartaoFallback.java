package com.mvp.cartoes.pagamento.cartao;

import java.io.IOException;

public class CartaoFallback implements CartaoServiceClient{

    private Exception cause;

    CartaoFallback(Exception cause) {
        this.cause = cause;
    }

    @Override
    public CartaoDto getByNumero(String numero) {
        if(cause instanceof IOException) {
            throw new RuntimeException("O serviço de cartao está offline");
        }
        return getCartaoDtoFake();
    }

    @Override
    public CartaoDto getById(Long id) {
        if(cause instanceof IOException) {
            throw new RuntimeException("O serviço de cartao está offline");
        }
        return getCartaoDtoFake();

    }

    private CartaoDto getCartaoDtoFake() {
        // Cliente fake
        CartaoDto cartaoDto = new CartaoDto();
        cartaoDto.setId(-1L);
        cartaoDto.setNumero(cause.getClass().getName());
        cartaoDto.setId(-1L);
        return cartaoDto;
    }

}
