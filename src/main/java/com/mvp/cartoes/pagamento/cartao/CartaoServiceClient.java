package com.mvp.cartoes.pagamento.cartao;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cartao",  configuration = CartaoConfiguration.class)
public interface CartaoServiceClient {

    @GetMapping("/cartao/{numero}")
    CartaoDto getByNumero(@PathVariable String numero);

    @GetMapping("/cartao/id/{id}")
    CartaoDto getById(@PathVariable Long id);

}
