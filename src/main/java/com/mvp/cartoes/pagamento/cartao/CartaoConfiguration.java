package com.mvp.cartoes.pagamento.cartao;

import feign.Feign;
import feign.codec.ErrorDecoder;
import io.github.resilience4j.feign.FeignDecorators;
import io.github.resilience4j.feign.Resilience4jFeign;
import org.springframework.context.annotation.Bean;

public class CartaoConfiguration {

    //captura os erros da api externa
    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new CartaoErrorDecoder();
    }

    //criar retorno padrão caso a api externa lance erro
    @Bean
    public Feign.Builder builder() {
        FeignDecorators decorators = FeignDecorators.builder()
                .withFallbackFactory(CartaoFallback::new)
                .build();

        return Resilience4jFeign.builder(decorators);
    }

}
