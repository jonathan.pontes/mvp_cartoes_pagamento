package com.mvp.cartoes.pagamento.cartao;

import com.mvp.cartoes.pagamento.utils.exceptions.CartaoNotFoundException;
import feign.Response;
import feign.codec.ErrorDecoder;
import org.hibernate.ObjectNotFoundException;

public class CartaoErrorDecoder implements ErrorDecoder {

    private ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if(response.status() == 404) {
            return new CartaoNotFoundException();
        }else{
            return errorDecoder.decode(s, response);
        }
    }
}
